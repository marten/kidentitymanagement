# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kidentitymanagement package.
#
# Andrej Vernekar <andrej.vernekar@moj.net>, 2007, 2008.
# Jure Repinc <jlp@holodeck1.com>, 2009.
# Andrej Mernik <andrejm@ubuntu.si>, 2014.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kidentitymanagement\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-25 00:45+0000\n"
"PO-Revision-Date: 2021-08-17 09:00+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"X-Generator: Lokalize 20.12.2\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"

#: core/identitymanager.cpp:165
#, kde-format
msgctxt "use default address from control center"
msgid "Default"
msgstr "Privzeto"

#: core/identitymanager.cpp:190
#, kde-format
msgctxt "Default name for new email accounts/identities."
msgid "Unnamed"
msgstr "Neimenovano"

#: core/identitymanager.cpp:368
#, kde-format
msgctxt ""
"%1: name; %2: number appended to it to make it unique among a list of names"
msgid "%1 #%2"
msgstr "%1 #%2"

#: core/identitymanager.cpp:562
#, kde-format
msgid "Unnamed"
msgstr "Neimenovano"

#: core/signature.cpp:169
#, kde-format
msgid "<qt>Failed to execute signature script<p><b>%1</b>:</p><p>%2</p></qt>"
msgstr ""
"<qt>Ni mogoče izvesti skripta za podpisovanje<p><b>%1</b>:</p><p>%2</p></qt>"

#: widgets/identitycombo.cpp:58
#, kde-format
msgctxt "Default identity"
msgid " (default)"
msgstr " (privzeto)"

#: widgets/signatureconfigurator.cpp:104
#, kde-format
msgid "&Enable signature"
msgstr "Omogoči podpis"

#: widgets/signatureconfigurator.cpp:106
#, kde-format
msgid ""
"Check this box if you want KMail to append a signature to mails written with "
"this identity."
msgstr ""
"Označite to polje, če želite, da KMail pripne podpis sporočilom, napisanim "
"pod to istovetnostjo."

#: widgets/signatureconfigurator.cpp:115
#, kde-format
msgid "Click on the widgets below to obtain help on the input methods."
msgstr "Kliknite na spodnje gradnike, da dobite pomoč o načinih vnosa."

#: widgets/signatureconfigurator.cpp:117
#, kde-format
msgctxt "continuation of \"obtain signature text from\""
msgid "Input Field Below"
msgstr "Vnosno polje spodaj"

#: widgets/signatureconfigurator.cpp:118
#, kde-format
msgctxt "continuation of \"obtain signature text from\""
msgid "File"
msgstr "Datoteka"

#: widgets/signatureconfigurator.cpp:119
#, kde-format
msgctxt "continuation of \"obtain signature text from\""
msgid "Output of Command"
msgstr "Izhod ukaza"

#: widgets/signatureconfigurator.cpp:120
#, kde-format
msgid "Obtain signature &text from:"
msgstr "Pridobi besedilo podpisa iz:"

#: widgets/signatureconfigurator.cpp:161
#, kde-format
msgid "Use this field to enter an arbitrary static signature."
msgstr "Uporabite to polje, da vnesete poljuben statičen podpis."

#: widgets/signatureconfigurator.cpp:202 widgets/signatureconfigurator.cpp:438
#, kde-format
msgid "&Use HTML"
msgstr "&Uporabi HTML"

#: widgets/signatureconfigurator.cpp:220
#, kde-format
msgid ""
"Use this requester to specify a text file that contains your signature. It "
"will be read every time you create a new mail or append a new signature."
msgstr ""
"Uporabite za določitev besedilne datoteke, ki vsebuje vaš podpis. Prebrana "
"bo vsakič, ko ustvarite novo sporočilo, ali pa pripnete nov podpis."

#: widgets/signatureconfigurator.cpp:223
#, kde-format
msgid "S&pecify file:"
msgstr "Navedite datoteko:"

#: widgets/signatureconfigurator.cpp:230
#, kde-format
msgid "Edit &File"
msgstr "Uredi datoteko"

#: widgets/signatureconfigurator.cpp:231
#, kde-format
msgid "Opens the specified file in a text editor."
msgstr "Odpre navedeno datoteko v urejevalniku besedila."

#: widgets/signatureconfigurator.cpp:251
#, kde-format
msgid ""
"You can add an arbitrary command here, either with or without path depending "
"on whether or not the command is in your Path. For every new mail, KMail "
"will execute the command and use what it outputs (to standard output) as a "
"signature. Usual commands for use with this mechanism are \"fortune\" or "
"\"ksig -random\". (Be careful, script needs a shebang line)."
msgstr ""
"Tu lahko dodate poljubne ukaze, z ali brez poti o tem, ali je ali ni ukaz že "
"v vaši poti. Za vsako novo pošto, bo KMail izvedel ukaz, izhod ukaza (na "
"standardni izhod) pa uporabil kot podpis. Običajni ukazi za uporabo s tem "
"mehanizmom sta \"fortune\" oz. \"ksig -random\". (Bodite previdni, scenarij "
"potrebuje vrstico shebang)."

#: widgets/signatureconfigurator.cpp:257
#, kde-format
msgid "S&pecify command:"
msgstr "Podajte ukaz:"

#: widgets/signatureconfigurator.cpp:417
#, kde-format
msgid "This text file size exceeds 1kb."
msgstr "Velikost te besedilne datoteke presega 1kb."

#: widgets/signatureconfigurator.cpp:417
#, kde-format
msgid "Text File Size"
msgstr "Velikost besedilne datoteke"

#: widgets/signatureconfigurator.cpp:448
#, kde-format
msgid "&Use HTML (disabling removes formatting)"
msgstr "&Uporabi HTML (onemogočitev odstrani oblikovanje sporočila)"
